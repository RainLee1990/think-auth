<?php
/*
* @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
* @Date: 2022-03-15 09:51:51
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-03-15 11:11:59
* @Description: File Description
*/

namespace rainlee\auth;

interface Authenticatable
{
    /**
     * 获取用户主键值
     *
     * @return mixed
     */
    public function getAuthIdentifier();

    /**
     * 获取用户密码
     *
     * @return string
     */
    public function getAuthPassword();

    /**
     * 获取Remember Token字段值.
     *
     * @return string
     */
    public function getRememberToken();

    /**
     * 为Remember Token字段赋值
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value);
}
