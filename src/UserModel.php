<?php
/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-15 10:27:31
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-03-25 15:40:55
 * @Description: 用户模型类
 */

namespace rainlee\auth;

use rainlee\auth\Authenticatable;
use think\Model;

class UserModel extends Model implements Authenticatable
{

    /**
     * 获取用户数据
     * 
     * @param array $where
     * @return \think\Model|null
     */
    public function getUserInfo($where = [])
    {
        return $this->where($where)->find();
    }

    /**
     * 获取用户主键值
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->{$this->getPk()};
    }

    /**
     * 获取用户密码
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * 获取Remember Token字段值.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * 为Remember Token字段赋值
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }
}
