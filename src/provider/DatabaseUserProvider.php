<?php

/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-10 15:18:38
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-04-01 16:36:16
 * @Description: File Description
 */

namespace rainlee\auth\provider;

use think\facade\Db;
use think\helper\Str;

class DatabaseUserProvider
{
    protected $config;


    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 根据ID获取用户数据
     *
     * @return 
     */
    public function retrieveById($id)
    {
        return $this->getUserInfo(['id' => $id]);
    }

    /**
     * 通过用户主键和remember token获取用户
     * 
     * @param int $id
     * @param string $token
     * 
     * @return
     */
    public function retrieveByRememberToken($id, $token)
    {
        $model = $this->retrieveById($id);

        if (!$model) {
            return;
        }

        return $model->remeber_token && hash_equals($model->remeber_token, $token) ? $model : null;
    }

    /**
     * 根据账号密码等参数获取用户
     * 
     * @param $credentials
     */
    public function retrieveByCredentials($credentials)
    {
        if (
            empty($credentials) ||
            (count($credentials) === 1 && array_key_exists('password', $credentials))
        ) {
            return;
        }

        $where = [];
        foreach ($credentials as $key => $value) {
            if (!Str::contains($key, 'password')) {
                $where[$key] = $value;
            }
        }

        return $this->getUserInfo($where);
    }

    /**
     * 验证用户密码是否正确
     * 
     * @param
     */
    public function validatePassword($user, $password)
    {
        if (isset($this->config['validate_pass']) && $this->config['validate_pass'] == 'md5') {
            return hash_equals($user->password, md5($password));
        }
        return password_verify($password, $user->password);
    }

    /**
     * 更新Remember Token
     * 
     * @param \think\model $user
     * @param string $toekn
     * @return void
     */
    public function updateRememberToken($user, $token)
    {
        $user->remember_token = $token;
        $user->save();
    }

    /**
     * 获取用户数据
     * 
     * @param array $where
     * @return \rainlee\auth\Authenticatable|null
     */
    protected function getUserInfo($where = [])
    {
        $user = $this->connect()->where($where)->find();

        if (!$user) {
            return null;
        }

        return $this->getGenericUser($user);
    }

    /**
     * 创建模型实例
     *
     * @param mixed $user
     * @return \rainlee\auth\Authenticatable
     */
    protected function getGenericUser($user)
    {
        if (!is_null($user)) {
            return new \rainlee\auth\GenericUser((array) $user);
        }
    }

    /**
     * 创建链接
     * 
     * @return \think\Db
     */
    protected function connect()
    {
        return Db::connect($this->config['connection'] ?? null)->table($this->config['table']);
    }
}
