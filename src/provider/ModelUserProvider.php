<?php

/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-10 15:18:38
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-04-01 16:53:38
 * @Description: File Description
 */

namespace rainlee\auth\provider;

use think\helper\Str;

class ModelUserProvider
{

    protected $config;


    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 根据ID获取用户数据
     *
     * @return 
     */
    public function retrieveById($id)
    {
        $model = $this->createModel();
        return $model->getUserInfo([$model->getPk() => $id]);
    }

    /**
     * 通过用户主键和remember token获取用户
     * 
     * @param int $id
     * @param string $token
     * 
     * @return
     */
    public function retrieveByRememberToken($id, $token)
    {
        $model = $this->createModel();

        $model = $model->getUserInfo([$model->getPk() => $id]);

        if (!$model) {
            return;
        }

        return $model->remeber_token && hash_equals($model->remeber_token, $token) ? $model : null;
    }

    /**
     * 根据账号密码等参数获取用户
     * 
     * @param $credentials
     */
    public function retrieveByCredentials($credentials)
    {
        if (
            empty($credentials) ||
            (count($credentials) === 1 && array_key_exists('password', $credentials))
        ) {
            return;
        }

        $where = [];
        foreach ($credentials as $key => $value) {
            if (!Str::contains($key, 'password')) {
                $where[$key] = $value;
            }
        }
        $model = $this->createModel();

        return $model->getUserInfo($where);
    }

    /**
     * 验证用户密码是否正确
     * 
     * @param
     */
    public function validatePassword($user, $password)
    {
        if (isset($this->config['validate_pass']) && $this->config['validate_pass'] == 'md5') {
            return hash_equals($user->password, md5($password));
        }
        return password_verify($password, $user->password);
    }

    /**
     * 更新Remember Token
     * 
     * @param \think\model $user
     * @param string $toekn
     * @return void
     */
    public function updateRememberToken($user, $token)
    {
        $user->remember_token = $token;
        $user->save();
    }

    /**
     * 创建模型实例
     *
     * @return \think\Model
     */
    protected function createModel()
    {
        $class = '\\' . ltrim($this->config['model'], '\\');

        return new $class;
    }
}
