<?php
/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-09 14:14:45
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-03-14 14:32:26
 * @Description: File Description
 */

namespace rainlee\auth;

use InvalidArgumentException;
use rainlee\auth\guard\JwtGuard;
use rainlee\auth\guard\SessionGuard;
use think\facade\Config;
use think\helper\Str;

class AuthManager
{

    protected $guards = [];

    /**
     * 获取看守器类
     * 
     * @param  string|null  $name
     * @return 
     */
    public function guard($name = null)
    {

        $name = $name ?: $this->getDefaultGuard();
        if (is_null($name)) {
            throw new InvalidArgumentException(sprintf(
                'Unable to resolve NULL guard for [%s].',
                static::class
            ));
        }

        return $this->guards[$name] ?? $this->guards[$name] = $this->resolve($name);
    }

    protected function resolveConfig($name)
    {
        return Config::get('auth.guards.' . $name);
    }

    /**
     * 创建驱动
     *
     * @param string $name
     * @return mixed
     *
     */
    protected function resolve(string $name)
    {
        $config = $this->resolveConfig($name);

        if (is_null($config)) {
            throw new InvalidArgumentException("Auth guard [{$name}] is not defined.");
        }

        $method = 'create' . Str::studly($config['driver']) . 'Driver';
        if (method_exists($this, $method)) {
            return $this->$method($name, $config);
        }

        throw new InvalidArgumentException("Auth guard driver [{$name}] is not defined.");
    }

    /**
     * 创建基于SESSION的认证看守器
     *
     * @param  string  $name
     * @param  array  $config
     * @return \rainlee\auth\SessionGuard
     */
    protected function createSessionDriver($name, $config)
    {
        return new SessionGuard($name, $config);
    }

    /**
     * 创建基于JWT的认证看守器
     *
     * @param  string  $name
     * @param  array  $config
     * @return \rainlee\auth\JwtGuard
     */
    protected function createJwtDriver($name, $config)
    {
        return new JwtGuard($name, $config);
    }

    public function getDefaultGuard()
    {
        return Config::get('auth.default');
    }

    /**
     * 动态调用
     * @param string $method
     * @param array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->guard()->$method(...$parameters);
    }
}
