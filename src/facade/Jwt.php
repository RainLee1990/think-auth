<?php
/*
* @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
* @Date: 2022-03-11 11:08:30
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-04-02 14:39:38
* @Description: File Description
*/

namespace rainlee\auth\facade;

use rainlee\auth\jwt\JwtManager;
use think\Facade;

/**
 * @see \rainlee\auth\JwtManager
 * @package think\facade
 * @mixin \rainlee\auth\JwtManager
 * @method static bool guard(null|string $name = null) 指定看守器
 * @method static string build(array $claims = []) 生成Token
 * @method static bool validation(null|string $token = null) 验证token
 * @method static array getClaims() 获取token荷载数据
 * @method static mixed getId() 获取唯一索引数据
 */
class Jwt extends Facade
{
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return JwtManager::class;
    }
}
