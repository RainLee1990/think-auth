<?php
/*
* @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
* @Date: 2022-03-11 11:08:30
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-03-15 11:31:55
* @Description: File Description
*/

namespace rainlee\auth\facade;

use rainlee\auth\AuthManager;
use rainlee\auth\guard\JwtGuard;
use rainlee\auth\guard\SessionGuard;
use think\Facade;

/**
 * @see \rainlee\auth\AuthManager
 * @package think\facade
 * @mixin \rainlee\auth\AuthManager
 * @method static bool attempt() 尝试登录
 * @method static string|null getDefaultDriver() 默认看守器
 * @method static SessionGuard|JwtGuard guard(null|string $name = null) 指定看守器
 */
class Auth extends Facade
{
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return AuthManager::class;
    }
}
