<?php
/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-25 15:12:33
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-03-25 15:40:37
 * @Description: 通用用户对象类
 */

namespace rainlee\auth;

class GenericUser implements Authenticatable
{
    /**
     * 用户的所有属性
     *
     * @var array
     */
    protected $attributes;

    /**
     * 创建通用用户对象
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * 获取用户主键名
     *
     * @return string
     */
    public function getPk()
    {
        return 'id';
    }

    /**
     * 获取用户主键值
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        $name = $this->getPk();

        return $this->attributes[$name];
    }

    /**
     * 获取用户密码
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->attributes['password'];
    }

    /**
     * 获取用户Remember Token值
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->attributes['remember_token'];
    }

    /**
     * 为Remember Token字段赋值
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->attributes['remember_token'] = $value;
    }

    /**
     * 动态访问用户属性
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->attributes[$key];
    }

    /**
     * 动态设置用户属性
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * 动态检查用户属性是否存在
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return isset($this->attributes[$key]);
    }

    /**
     * 动态取消用户属性的值
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->attributes[$key]);
    }
}
