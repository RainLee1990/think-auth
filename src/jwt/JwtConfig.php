<?php
/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-28 18:39:15
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-04-01 15:52:50
 * @Description: File Description
 */

declare(strict_types=1);

namespace rainlee\auth\jwt;

use InvalidArgumentException;
use Lcobucci\JWT\Signer;

class JwtConfig
{
    // 唯一索引
    protected $unique_index = 'id';
    // 私钥
    protected $signer_key = null;
    // Token生成后的生效时间
    protected $effective_at = 0;
    // Token有效期（秒
    protected $expires_at = 3600;
    // 刷新时间
    protected $refresh_ttL = 7200;
    // 加密方式
    protected $signer = \Lcobucci\JWT\Signer\Hmac\Sha256::class;
    // 获取 Token 方式，数组靠前值优先
    protected $token_mode = ['header', 'cookie', 'param'];
    // Token过期抛异常code
    protected $relogin_code = 4011;
    // 刷新Token过期抛异常code
    protected $refresh_code = 4012;
    // jwt签发者
    protected $iss = '';
    // 接收jwt的一方
    protected $aud = '';
    // 验证IP
    protected $verify_ip = false;

    public function __construct(array $options)
    {
        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * 获取私钥
     * @return string
     */
    public function getSignerKey()
    {
        if (empty($this->signer_key)) {
            throw new InvalidArgumentException('config signer_key required.');
        }

        return $this->signer_key;
    }

    /**
     * 获取唯一索引字段名
     * @return string
     */
    public function getUniqueIndex(): string
    {
        return $this->unique_index;
    }

    /**
     * 获取Token生成后多久生效
     * @return int
     */
    public function getEffectiveAt(): int
    {
        return $this->effective_at;
    }

    /**
     * 获取Toekn过期时长
     * @return int
     */
    public function getExpiresAt(): int
    {
        return $this->expires_at;
    }

    /**
     * 获取Toekn刷新时长
     * @return int
     */
    public function getRefreshTTL(): int
    {
        return $this->refresh_ttL;
    }

    /**
     * 获取jwt签发人
     * @return string
     */
    public function getIss(): string
    {
        return $this->iss;
    }

    /**
     * 获取jwt接收方
     * @return string
     */
    public function getAud(): string
    {
        return $this->aud;
    }

    /**
     * 获取加密类
     * @return Signer
     */
    public function getSigner(): Signer
    {
        return new $this->signer;
    }

    /**
     * 获取 Token 方式，数组靠前值优先
     * @return array
     */
    public function getTokenModel(): array
    {
        return $this->token_mode;
    }

    /**
     * 获取Token过期抛异常code
     * @return int
     */
    public function getReloginCode(): int
    {
        return $this->relogin_code;
    }

    /**
     * 获取Token失效抛异常code
     * @return int
     */
    public function getRefreshCode(): int
    {
        return $this->refresh_code;
    }

    /**
     * 获取是否验证IP
     * @return bool
     */
    public function getVerifyIP()
    {
        return $this->verify_ip;
    }
}
