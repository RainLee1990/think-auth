<?php
/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-15 15:07:05
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-03-15 15:22:14
 * @Description: File Description
 */

declare(strict_types=1);

namespace rainlee\auth\middleware;

use rainlee\auth\facade\Auth;

class RedirectIfAuthenticated
{

    protected $userHome = '/home';

    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect($this->userHome);
        }

        return $next($request);
    }
}
