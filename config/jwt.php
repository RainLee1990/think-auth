<?php

return [
    'default' => 'admin', //默认场景
    'guards' => [
        'api' => [
            'unique_index'  => 'id', // 唯一索引字段名
            'signer_key'    => env('app.app_key'), // 密钥
            'effective_at'  => 0, // 生成后多久生效，默认生成后直接使用
            'expires_at'    => 7200, // Token有效期（秒
            'refresh_ttL'   => 7200,
            'signer'        => 'Lcobucci\JWT\Signer\Hmac\Sha256', // 加密算法
            'token_mode'    => ['header', 'cookie', 'param'], // 获取 Token 方式，数组靠前值优先
            'iss'           => '',
            'aud'           => '',
            'verify_ip'     => true  // 验证IP
        ],
        'admin' => [
            'unique_index'  => 'uid', // 唯一索引字段名
            'signer_key'    => env('app.app_key'), // 密钥
            'effective_at'  => 0, // 生成后多久生效，默认生成后直接使用
            'expires_at'    => 3600, // Token有效期（秒
            'refresh_ttL'   => 7200,
            'signer'        => 'Lcobucci\JWT\Signer\Hmac\Sha256', // 加密算法
            'token_mode'    => ['header', 'cookie', 'param'], // 获取 Token 方式，数组靠前值优先
            'iss'           => '',
            'aud'           => '',
            'verify_ip'     => false  // 验证IP
        ]
    ]
];
