<?php
/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-03-09 14:14:45
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-04-14 17:45:16
 * @Description: File Description
 */

return [
    'default' => 'web',
    'app_key' => env('app.app_key'),
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'user'
        ],
        'api' => [
            'driver' => 'jwt',
            'provider' => 'user',
            'ignored'  => [ // 无需登录认证的节点
                'auth:login'
            ],
            'payload'  => [ // jwt荷载(Payload)部分存入的数据字段名，driver为jwt时有效，设置不存在则默认为主键
                'id',
                'username'
            ]
        ],
        'admin' => [
            'driver' => 'session',
            'provider' => 'admin',
            'policies' => 'admin',
            'ignored'  => [ // 无需登录认证的节点
                'auth:login'
            ],
        ]
    ],

    'providers' => [
        'user' => [
            'driver' => 'model',
            'model' => app\admin\model\Users::class,
        ],
        'admin' => [
            'driver' => 'database',
            'table'  => 'users',
            'connection' => 'mysql', // database配置文件数据库链接名忽略为默认数据库链接
            'validate_pass' => 'md5' // 密码验证方式，默认password_verify
        ]
    ],

    'policies' => [ // 权限认证策略
        'admin' => [
            'auth_on'     => true,          // 认证开关
            'type'        => 1,
            'super_admin' => 1,  // 超级管理员id
            'groups'      => 'think_auth_group', //用户组数据表名
            'group_rule'  => 'think_auth_group_rule', // 用户组权限节点关联表
            'rules'       => 'think_auth_rule', //权限规则表
            'ignored'     => [  // 无需认证的节点

            ]
        ]
    ]
];
